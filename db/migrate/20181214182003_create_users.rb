class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.integer :phonenumber
      t.string :address
      t.boolean :is_admin, default: false
      t.timestamps
    end
  end
end
