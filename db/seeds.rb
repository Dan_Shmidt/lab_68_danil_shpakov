# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
user = User.create(
    name: 'Василий',
    phonenumber: '+77894561234',
    address: 'Гоголя 45 кв 12',
    password: 123456,
    email: 'vasya@gmail.com'
)
user = User.create(
    name: 'Пётр',
    phonenumber: '+77894561234',
    address: 'Гоголя 46 кв 22',
    password: 123456,
    email: 'petya@gmail.com'
)
user = User.create(
    name: 'Николай',
    phonenumber: '+77894561234',
    address: 'Гоголя 45 кв 2',
    password: 123456,
    email: 'kolya@gmail.com'
)
user = User.create(
    name: 'Вера',
    phonenumber: '+77894561234',
    address: 'Гоголя 45 кв 27',
    password: 123456,
    email: 'vera@gmail.com'
)
user = User.create(
    name: 'Илона',
    phonenumber: '+77894561234',
    address: 'Гоголя 45 кв 58',
    password: 123456,
    email: 'ilona@gmail.com'
)