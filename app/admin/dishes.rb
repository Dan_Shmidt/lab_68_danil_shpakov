ActiveAdmin.register Dish do
    form do |f|
        f.inputs do
            if object.picture.attached?
                f.input :picture,
                :as => :file,
                :hont => image_tag(
                    url_for(
                        f.object.picture.variant(combine_options: { gravity: 'Center' })
                    )
                )
            else
                f.input :picture, :as => :file
            end
            f.input :foodcaurt_id, label: 'Foodcaurt', as: :select, collection: Foodcaurt.all.map{ |place| [place.name, place.id] }
            f.input :name
            f.input :price
            f.input :desc
        end
        f.actions
    end

    index do
        selectable_column
        id_column
            column :picture do |dish|
                if dish.picture.attached?
                    image_tag dish.picture.variant(combine_options: { gravity: 'Center'})
                end
            end
        column :name do |dish|
            link_to dish.name, admin_dish_path(dish)
        end
        column :price
    end

    show do
        attributes_table do
            row :picture do |dish|
                image_tag dish.picture.variant(combine_options: { gravity: 'Center' })
            end
        row :desc
        row :price
        end
        active_admin_comments
    end


    permit_params :picture, :desc, :name, :price, :foodcaurt_id

end
