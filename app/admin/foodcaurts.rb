ActiveAdmin.register Foodcaurt do
    form do |f|
        f.inputs do
            if object.picture.attached?
                f.input :picture,
                :as => :file,
                :hont => image_tag(
                    url_for(
                        f.object.picture.variant(combine_options: { gravity: 'Center' })
                    )
                )
            else
                f.input :picture, :as => :file
            end
            f.input :name
            f.input :desc
        end
        f.actions
    end

    index do
        selectable_column
        id_column
            column :picture do |foodcaurt|
                if foodcaurt.picture.attached?
                    image_tag foodcaurt.picture.variant(combine_options: { gravity: 'Center'})
                end
            end
        column :name do |foodcaurt|
            link_to foodcaurt.name, admin_foodcaurt_path(foodcaurt)
        end
    end

    show do
        attributes_table do
            row :picture do |foodcaurt|
                image_tag foodcaurt.picture.variant(combine_options: { gravity: 'Center' })
            end
        row :name
        row :desc
        end
        active_admin_comments
    end

    permit_params :picture, :desc, :name
end
