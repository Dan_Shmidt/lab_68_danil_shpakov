class Dish < ApplicationRecord
  belongs_to :foodcaurt
  has_one_attached :picture
  has_many :carts
end
