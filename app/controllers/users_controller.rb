class UsersController < ApplicationController
    def home
        if !user_signed_in?
            redirect_to new_user_session_path
        else
            redirect_to users_path
        end
    end
end
