class ApplicationController < ActionController::Base
    before_action :configure_permitted_parameters, if: :devise_controller?

    def access_denied(exception)
        flash[:danger] = exception.message
        redirect_to root_url
    end

    private

    def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:name,:phonenumber, :address])
        devise_parameter_sanitizer.permit(:account_update, keys: [:name,:phonenumber, :address])
    end

    def after_sign_in_path_for(resource)
        current_user
    end
end
