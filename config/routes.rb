Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  root 'foodcaurts#index'
  devise_for :users
  resources :users, only:[:show, :index]
  resources :foodcaurts, only:[:show, :index]
  resources :dishes, only:[:show]
end
